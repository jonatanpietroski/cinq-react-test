export interface Person {
    id: number;
    firstName: string;
    lastName: string;
    description: string;
    age: number;

    constructor() 
}
