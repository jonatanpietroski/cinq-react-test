import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from 'src/app/interfaces/person/person';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private personSubject = new BehaviorSubject({});
  private peopleSubject = new BehaviorSubject({});
  currentPerson = this.personSubject.asObservable();
  people = this.peopleSubject.asObservable();

  constructor(private http: HttpClient) {}

  /**
   * @description Sets a new person into the Observable.
   * @param person person to be set in cache through an Observable.
   */
  setPerson(person: Person) {
    this.personSubject.next(person);
  }

  /**
   * @description Sets an Array of Person to be used as a cached data through the user session.
   * @param people Array of Person that is cached through an Observable.
   */
  setPeopleCache(people: Array<Person>) {
    this.peopleSubject.next(people);
  }

  /**
   * @description Fetches user data from server
   */
  fetchUsers() {
    this.people.subscribe((peopleData:Array<Person>) => {
      if(!peopleData.length) {
        this.http.get('assets/resources/users.json').subscribe((data:Array<Person>) => {
          this.peopleSubject.next(data);
        });
      }       
    }).unsubscribe();
    
  }

  /**
   * @description Searches for a match of the string in the Array of Person.
   * @param string Search string
   * @param tmpPersonArray Array of person with all the original records.
   */
  searchForPerson(string: string, tmpPersonArray: Array<Person>) {
    let reg = new RegExp(string, 'gi');
    if (string != null || string != '') {
      let array = tmpPersonArray.filter(val => {
        if(reg.test(val.firstName) || reg.test(val.lastName) || reg.test(String(val.id)) || reg.test(String(val.age))) {
          return true;
        }
      });
      return array; //Returns the temporary Array with the search results.
    } else { 
      return tmpPersonArray; //If none is found, returns the original Array.
    }
  }

  /**
   * @description Downloads the data from an Array of Person.
   * @param headers The Headers of the csv file.
   * @param people Main data to be shown as the body of the csv.
   */
  download(headers: Array<string>, people: Array<Person>) {
    let csv = ""; //Init csv data.
    let csvHeaders = Array.from(headers); //Creates a new instance of the headers Array.
    csvHeaders.shift(); //Removes the first element (ID)
    csvHeaders.pop(); //Removes the (Action) Header
    csvHeaders.push('Description'); //Adds the Description Header.
    csvHeaders.forEach((headerName, index) => {
      csv += headerName + (index+1 < csvHeaders.length ? ';' : '\r\n' );
    });
    var BOM = "\uFEFF"; //Doesn't open the csv file without it.
    csv = BOM + csv; //Adds the BOM.

    //Here the main data of the csv file is built.
    people.forEach((person, peopleIndex) => {
      let keys = Object.keys(person);
      keys.forEach((key,keyIndex) => {
        csv += people[peopleIndex][key] + (keyIndex+1 < keys.length ? ';' : '\r\n' )
      })
    })

    
    // Once we are done looping, download the .csv by creating a link.
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv';
    hiddenElement.click();
  }

  
}
