import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { PersonService } from 'src/app/services/person/person.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Person } from 'src/app/interfaces/person/person';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import * as _ from "lodash";
import { ElementFinder } from 'protractor';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  personSubscriber: Subscription;
  currentPersonSubscriber: Subscription;
  people: Array<Person>; //Array of people to be shown in the component.
  peopleSelected: Array<Person> = []; //Array of Person that were selected.
  searchData: Array<Person> = []; //Temporary Array that stores the original array data fetched from the server.
  searchString: string = ""; //Search String.
  tableHeaders: Array<string> = ['Select', 'ID', 'First Name', 'Last Name', 'Age', 'Actions']; //Array of Headers.
  showGrid: boolean = false; //Show grid form.

  constructor(private personService: PersonService, private router: Router,private elRef: ElementRef) {}
  
  ngOnInit() {
    //Resizes the window
    this.windowWidth();    
    //Once the app initializes, we fetch all data and store it in cached memory.
    this.personSubscriber = this.personService.people.pipe(map((data: Person[]) => Array.from(data))).subscribe(data => {
      this.people = data;
      this.searchData = data;
    })
   this.personService.fetchUsers()
  }
  
  ngOnDestroy() {
    //Unsubscribe
    this.personSubscriber.unsubscribe();
  }


  //Listens for window changes, in order to show the data in a grid.
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.windowWidth();    
  }

  /**
   * @description checks the size of the window, in order to show the data in a grid.
   */
  windowWidth() {
    if(window.innerWidth < 1024) {
      this.showGrid = true;
    } else {
      this.showGrid = false;
    }
  }

  //Download the data in a .csv file.
  download() {
    this.personService.download(this.tableHeaders, this.people);   
  }

  /**
   * 
   * @param string string that will search for the match in the array.
   */
  search(string: string) {
    this.people = this.personService.searchForPerson(string, this.searchData);
  }

  /**
   * @description show details of due person.
   * @param person Person to be shown.
   */
  show(person: Person) {
    this.personService.setPerson(person);
    this.router.navigate(['person']);
  }


  /**
   * @description deletes a person from the array of people.
   * @param person person to be deleted.
   * @param isMultidelete rather is a multi-deleate selection or not.
   */
  delete(person: Person = null, isMultidelete = false) {
    swal({
      text: "Are you sure you want to delete?",
      confirmButtonText: "Yes",
      showCancelButton: true,
      cancelButtonText: "Cancel",
      type: "question"
    }).then(done => {
      if (done.value) {
        let newArray;
        if(!isMultidelete) {
          newArray = _.remove(this.people, val => {
              return person.id != val.id;
          })
        } else {
          newArray = this.people.filter(person => {
            return this.peopleSelected.indexOf(person) == -1;            
          });
          this.peopleSelected = [];
        }
        this.personService.setPeopleCache(newArray);
      }
    })
  }

  /**
   * @description Adds a person to an Array of selected people.
   * @param person Person that will be added to the selected people Array.
   */
  selectCheckbox(person: Person) {
    let index = this.peopleSelected.indexOf(person);
    if(index == -1) {
      this.peopleSelected.push(person); //Adds person to the Array.
    } else {
      this.peopleSelected.splice(index, 1); //Removes the person from array.
    }
  }

}
