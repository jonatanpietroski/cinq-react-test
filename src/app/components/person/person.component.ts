import { Component, OnInit } from '@angular/core';
import { PersonService } from 'src/app/services/person/person.service';
import { Person } from 'src/app/interfaces/person/person';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  person: Person; //Object that displays the content on the component.
  personFormGroup: FormGroup;
  constructor(private personService: PersonService, private router: Router) {}

  ngOnInit() {
    this.personService.currentPerson.subscribe((person: Person) => {
      this.person = person;
      this.initFormGroup();
      if(this.person.id == null || typeof this.person.id === 'undefined') {
        this.router.navigate(['dashboard']);
      }
      
    });
  }

  /**
   * @description Initializes the Form Group
   */
  initFormGroup() {
    this.personFormGroup = new FormGroup({
      firstName: new FormControl(this.person.firstName, Validators.compose([Validators.required,Validators.pattern("^[a-zA-Zç' ]+$")])),
      lastName: new FormControl(this.person.lastName, Validators.compose([Validators.required,Validators.pattern("^[a-zA-Zç' ]+$")])),
    })
  }

  /**
   * @description Checks if the user have changed the input contents. If he did change any of the input contents,
   * he is asked if the wants to cancel the operation, if so, the form is reinitialized with the original values.
   * @param form form containing the content of the "First Name", and "Last Name of the user"
   */
  cancel(form:FormGroup) {
    
    if(form.controls.firstName.touched || form.controls.lastName.touched) {
      let that = this;
      swal({
        type:'question',
        text:'Are tou sure you want to cancel? All changes will be lost!',
        confirmButtonText: "Yes",
        showCancelButton: true,
        cancelButtonText: "No"
      }).then(done => {
        if(done.value) {
          that.initFormGroup(); //Initializes the form group again with default values of the person object.
        }
      })
    }
  }

  /**
   * @description Saves a Person with the new values of the inputs.
   * @param form form containing the content of the "First Name", and "Last Name of the user"
   */
  save(form: FormGroup, save = true) {
    this.personService.people.subscribe((data:Array<Person>) => {
      let index = data.indexOf(this.person);
      if(data[index].firstName != form.value.firstName || data[index].lastName != form.value.lastName && save) {
        data[index].firstName = form.value.firstName;
        data[index].lastName = form.value.lastName;
        console.log(data[index]);
        swal({
          type: 'success',
          title:"Changed",
          text: 'Person changed successfully.',
          showConfirmButton: true,
          confirmButtonText: 'OK'
        }).then(done => {
          this.router.navigate(['dashboard']);
        });
      }
    }).unsubscribe();
    
    //TODO Save the data.
    
  }

}
