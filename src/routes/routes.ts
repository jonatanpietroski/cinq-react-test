import { Routes} from "@angular/router"
import { DashboardComponent } from "src/app/components/dashboard/dashboard.component";
import { PersonComponent } from "src/app/components/person/person.component";
import { NotFoundComponent } from "src/app/components/not-found/not-found.component";

export const APPLICATION_ROUTES: Routes = [
    //Routes
    { path: 'dashboard', component: DashboardComponent },
    
    //Person
    { path: 'person', component: PersonComponent},
    { path: 'person/:id', component: PersonComponent},
    
    //Redirects
    { path: 'not-found', component: NotFoundComponent},
    { path: '', redirectTo:'dashboard', pathMatch: 'full'},
    { path: '**', redirectTo:'not-found', pathMatch: 'full'}
];