# CinqReactTest

In order to run this project, make sure you have NodeJs, NPM, and the Angular CLI  installed on your computer. You may find instructions in the
following website [Getting Started](https://angular.io/guide/quickstart) on how to install the Angular CLI and it's dependencies.

##Dependencies installation

Once you have installed the Angular CLI, and downloaded the project, it is time to install all the project dependencies. Open the project folter inside of a terminal window, or command prompt window, and run the following command:

`npm i`

This command will install all the project dependencies we need to run the project correctly.

## Development server

Run `ng serve --open` for a dev server. This command will run a development server on your browser, it should open your default browser once it is ready. If in any case it does not show up, Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).