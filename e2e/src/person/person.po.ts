import { browser, by, element } from 'protractor';

export class PersonPage {
  navigateTo() {
    return browser.get('/person');
  }

  get firstNameInput() {
    return element(by.id('firstNameInput'));
  }

  get lastNameInput() {
    return element(by.id('lastNameInput'));
  }

  get errorMesageFirstName() {
    return element(by.id('specialCharacters'));
  }

  get saveBtn() {
    return element(by.id('saveBtn'));
  }
}
