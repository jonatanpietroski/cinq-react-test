import {browser, by, protractor} from 'protractor';
import { PersonPage } from "./person.po";
import { DashboardPage } from '../dashboard/dashboard.po';

describe('Person Page', () => {
    let dash: DashboardPage;
    let person: PersonPage;
    const EC = protractor.ExpectedConditions;

    beforeEach(() => {
        dash = new DashboardPage();
        person = new PersonPage();
    });

    it('should navigate to the person page', () => {
        browser.get('/dashboard');
        browser.sleep(2000);
        //browser.wait(EC.visibilityOf(dash.));
        dash.showPersonBtn.then(row => {
            row[0].element(by.id('showPersonBtn')).click();
            browser.wait(EC.visibilityOf(person.firstNameInput));
            expect(EC.visibilityOf(person.firstNameInput));
            browser.sleep(2000);    
        });
    });
    
    it('should change the first and last name and save them',  () => {
        browser.wait(EC.visibilityOf(person.firstNameInput));
        console.log(EC.visibilityOf(person.firstNameInput));        
        expect(EC.visibilityOf(person.firstNameInput));
        person.firstNameInput.clear();
        person.firstNameInput.sendKeys('Steve');
        person.lastNameInput.clear();
        person.lastNameInput.sendKeys('Nash');
        person.saveBtn.click();
        browser.sleep(2000);
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
        console.log(EC.visibilityOf(person.firstNameInput)); 
    });

    it('should have changed the first name and last name', () => {
        browser.sleep(1000);  
        browser.wait(EC.visibilityOf(dash.showPersonPeople));
        dash.mainDataElement.then(row => {
            let firstName = row[0].element(by.id('firstName'));
            let lastName = row[0].element(by.id('lastName'));
            expect(firstName.getText()).toEqual('Steve');
            expect(lastName.getText()).toEqual('Nash');
        });
        browser.sleep(3000);
    });
});