import { browser, by, element } from 'protractor';

export class DashboardPage {
  navigateTo() {
    return browser.get('/dashboard');
  }

  get showPersonBtn() {
      return element.all(by.id('showPersonBtnParent'));
  }

  get showPersonPeople() {
    return element(by.id('people'));
  }

  get mainDataElement() {
    return element.all(by.id('mainDataElement'));
  }
}
